/*
 * analog.c
 *
 *  Created on: Jan 10, 2021
 *      Author: marci
 */

#include "FreeRTOS.h"
#include "cmsis_os.h"
#include "shell.h"
#include "task.h"
#include "semphr.h"
#include "string.h"

#include "analog.h"

#define AD_CHANNELS (2)
#define BOYLER_CHANNEL (0)
#define FLOOR_CHANNEL (1)

#define AD_MAX_VALUE (4095)
#define AD_MIN_VALUE (0)
#define MAX_TEMPERATURE (100)
#define MIN_TEMPERATURE (0)

#define MOVING_HYSTERESIS_DELTA (20)
#define MOVING_AVERAGE_QUEUE_SIZE (10)

#define NOTIFY_WAIT_MS (100)
#define CONV_INTERVAL_MS (20)
#define TASK_AD_SLEEP_TIME_MS (100)

#define EVT_AD_CONV_END (1<<0)
#define EVT_AD_CONV_START (1<<1)
#define EVT_AD_CONV_STOP (1<<2)

#define EVT_AD_MASK ((EVT_AD_CONV_START) | (EVT_AD_CONV_STOP) | (EVT_AD_CONV_END))

typedef struct {
	uint32_t delta;
	uint32_t hyst_min;
	uint32_t hyst_max;
} moving_hysteresis_t;

typedef struct {
	uint32_t queue[MOVING_AVERAGE_QUEUE_SIZE];
	uint8_t index;
	uint32_t average;
} moving_average_t;

typedef struct {
	uint8_t boyler_temperature;
	uint8_t floor_temperature;
} temperature_t;

typedef struct {
	volatile uint8_t ad_running;
	uint32_t ad_raw_values[AD_CHANNELS];
	uint32_t ad_current_values[AD_CHANNELS];
	moving_hysteresis_t moving_hysteresis[AD_CHANNELS];
	moving_average_t moving_average[AD_CHANNELS];
	temperature_t temperature;
} ad_task_ctrl_t;

static ad_task_ctrl_t ad_task_ctrl;

extern ADC_HandleTypeDef hadc1;
extern osThreadId_t adTaskHandle;
extern osMutexId_t mtx_adHandle;

BaseType_t ad_init(){
	bzero(&ad_task_ctrl.ad_raw_values, sizeof(ad_task_ctrl.ad_raw_values));
	bzero(&ad_task_ctrl.ad_current_values, sizeof(ad_task_ctrl.ad_current_values));

	for(int i = 0; i < AD_CHANNELS; i++){
		ad_task_ctrl.moving_hysteresis[i].hyst_min = 0;
		ad_task_ctrl.moving_hysteresis[i].hyst_max = MOVING_HYSTERESIS_DELTA;
		ad_task_ctrl.moving_hysteresis[i].delta = MOVING_HYSTERESIS_DELTA;
	}

	bzero(&ad_task_ctrl.moving_average, sizeof(ad_task_ctrl.moving_average));
	bzero(&ad_task_ctrl.temperature, sizeof(ad_task_ctrl.temperature));

	return pdPASS;
}

BaseType_t ad_deinit(){
	return pdPASS;
}

static uint32_t map(uint32_t x, uint32_t x1, uint32_t x2, uint32_t y1, uint32_t y2)
{
	return (x - x1) * (y2 - y1) / (x2 - x1) + y1;
}

static uint32_t add_moving_hysteresis(uint8_t channel, uint32_t input){

		if(input > ad_task_ctrl.moving_hysteresis[channel].hyst_max){
			ad_task_ctrl.moving_hysteresis[channel].hyst_max += ad_task_ctrl.moving_hysteresis[channel].delta;
			ad_task_ctrl.moving_hysteresis[channel].hyst_min += ad_task_ctrl.moving_hysteresis[channel].delta;
			return ad_task_ctrl.moving_hysteresis[channel].hyst_max;
		}

		if(input < ad_task_ctrl.moving_hysteresis[channel].hyst_min &&
				((ad_task_ctrl.moving_hysteresis[channel].hyst_min - ad_task_ctrl.moving_hysteresis[channel].delta) > 0)){
			ad_task_ctrl.moving_hysteresis[channel].hyst_max -= ad_task_ctrl.moving_hysteresis[channel].delta;
			ad_task_ctrl.moving_hysteresis[channel].hyst_min -= ad_task_ctrl.moving_hysteresis[channel].delta;
			return ad_task_ctrl.moving_hysteresis[channel].hyst_min;
		}

	return input;
}

static void add_moving_average(uint8_t channel, uint32_t value){

	ad_task_ctrl.moving_average[channel].queue[ad_task_ctrl.moving_average[channel].index] = value;
	ad_task_ctrl.moving_average[channel].index = (ad_task_ctrl.moving_average[channel].index + 1) % MOVING_AVERAGE_QUEUE_SIZE;

	uint32_t sum = 0;
	for (int i = 0; i < MOVING_AVERAGE_QUEUE_SIZE; i++){
		sum += ad_task_ctrl.moving_average[channel].queue[i];
	}

	ad_task_ctrl.moving_average[channel].average = sum / MOVING_AVERAGE_QUEUE_SIZE;
}

static void start_ad_conversion(){

	BaseType_t result = HAL_ADC_Start_DMA(&hadc1, ad_task_ctrl.ad_raw_values, AD_CHANNELS);

	if(result != HAL_OK){
		pr_err("ad converter start conversion error");
	}
	else{
		ad_task_ctrl.ad_running = 1;
	}
	return;
}

//IT
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc){
	BaseType_t hptw = pdFALSE;
	xTaskNotifyFromISR(adTaskHandle, EVT_AD_CONV_END, eSetBits, &hptw);
	portYIELD_FROM_ISR(&hptw);
}

//IT
void HAL_ADC_ErrorCallback(ADC_HandleTypeDef *hadc){
	HAL_ADC_Stop_DMA(&hadc1);
}

void ad_processing_task(void const *argument){
	uint32_t notified_value;
	BaseType_t result;

	for(;;){

		result = xTaskNotifyWait(EVT_AD_MASK, EVT_AD_MASK, &notified_value, pdMS_TO_TICKS(NOTIFY_WAIT_MS));

		if(notified_value & EVT_AD_CONV_STOP){
			ad_task_ctrl.ad_running = 0;
			result = HAL_ADC_Stop_DMA(&hadc1);
			if(result != HAL_OK){
				pr_err("AD stop error");
			}
		}

		if(notified_value & EVT_AD_CONV_START){
			start_ad_conversion();
		}

		if(notified_value & EVT_AD_CONV_END){
			if(xSemaphoreTake(mtx_adHandle, pdMS_TO_TICKS(NOTIFY_WAIT_MS)) == pdTRUE){
				for (int i = 0; i < AD_CHANNELS; i++){
					add_moving_average(i, add_moving_hysteresis(i, ad_task_ctrl.ad_raw_values[i]));
					ad_task_ctrl.ad_current_values[i] = ad_task_ctrl.moving_average[i].average;
				}
				ad_task_ctrl.temperature.boyler_temperature =
						map(ad_task_ctrl.ad_current_values[BOYLER_CHANNEL], AD_MIN_VALUE, AD_MAX_VALUE, MIN_TEMPERATURE, MAX_TEMPERATURE);
				ad_task_ctrl.temperature.floor_temperature =
						map(ad_task_ctrl.ad_current_values[FLOOR_CHANNEL], AD_MIN_VALUE, AD_MAX_VALUE, MIN_TEMPERATURE, MAX_TEMPERATURE);
				xSemaphoreGive(mtx_adHandle);
			}

			osDelay(CONV_INTERVAL_MS);

			if(!ad_task_ctrl.ad_running){
				continue;
			}

			start_ad_conversion();
			continue;
		}
	}
}

uint8_t ad_get(uint32_t *result, uint8_t channel, TickType_t wait){
	assert_param(result);
	assert_param(channel < AD_CHANNELS);

	if(xSemaphoreTake(mtx_adHandle, wait) == pdTRUE){
		if(!channel){
			*result = ad_task_ctrl.temperature.boyler_temperature;
		}
		else{
			*result = ad_task_ctrl.temperature.floor_temperature;
		}
		xSemaphoreGive(mtx_adHandle);
		return pdPASS;
	}

	return pdFALSE;
}

BaseType_t temperature_get(uint8_t *result, uint8_t sensor, TickType_t wait){
	assert_param(result);
	assert_param(sensor < 2);

	if(xSemaphoreTake(mtx_adHandle, wait) == pdTRUE){

		if(!sensor){
			*result = ad_task_ctrl.temperature.boyler_temperature;
		}
		else{
			*result = ad_task_ctrl.temperature.floor_temperature;
		}

		xSemaphoreGive(mtx_adHandle);
		return pdPASS;

	}

		return pdFALSE;
}

#define CMD_HELP "help"
#define CMD_START "start"
#define CMD_STOP "stop"
#define CMD_STAT "stat"

static void cmd_usage(){
	sh_printf("usage %s <help | start | stop | stat>\r\n", TASK_COMMAND_AD);
}

void cmd_analog(int argc, char **argv){

	if(argc <= 0 || strcasecmp(CMD_HELP, argv[0]) == 0){
		cmd_usage();
		return;
	}

	if(strcasecmp(CMD_STAT, argv[0]) == 0){

		sh_printf("raw channels: ");
		for(int i=0; i<AD_CHANNELS; i++){
			sh_printf("CH%d: %d ", i, ad_task_ctrl.ad_raw_values[i]);
		}

		sh_printf("\r\nnormalized values: ");
		for(int i=0; i<AD_CHANNELS; i++){
			sh_printf("CH%d: %d ", i, ad_task_ctrl.ad_current_values[i]);
		}

		sh_printf("\r\n");

		return;
	}

	if(strcasecmp(CMD_START, argv[0]) == 0){

		if(ad_task_ctrl.ad_running){
			sh_printf("ad task already running\r\n");
			return;
		}

		xTaskNotify(adTaskHandle, EVT_AD_CONV_START, eSetBits);
		sh_printf("ad task is starting\r\n");
		return;
	}

	if(strcasecmp(CMD_STOP, argv[0]) == 0){

			if(!ad_task_ctrl.ad_running){
				sh_printf("ad task already stopped\r\n");
				return;
			}

			xTaskNotify(adTaskHandle, EVT_AD_CONV_STOP, eSetBits);
			sh_printf("ad task is stopping\r\n");
			return;
		}

	cmd_usage();
	return;
}

#define CMD_BOYLER "boyler"
#define CMD_FLOOR "floor"

void temp_usage(){
	sh_printf("usage %s <help | boyler | floor>\r\n", TASK_COMMAND_TEMP);
}

void cmd_temp(int argc, char **argv){

	if(argc <= 0 || strcasecmp(CMD_HELP, argv[0]) == 0){
		temp_usage();
		return;
	}

	if(strcasecmp(CMD_BOYLER, argv[0]) == 0){
		sh_printf("boyler temperature: %d°C\r\n", ad_task_ctrl.temperature.boyler_temperature);
		return;
	}

	if(strcasecmp(CMD_FLOOR, argv[0]) == 0){
		sh_printf("floor temperature: %d°C\r\n", ad_task_ctrl.temperature.floor_temperature);
		return;
	}

	temp_usage();
	return;
}
