/*
 * pump.c
 *
 *  Created on: Jan 10, 2021
 *      Author: marci
 */

#include "FreeRTOS.h"
#include "cmsis_os.h"
#include "shell.h"
#include "task.h"
#include "semphr.h"
#include "string.h"

#include "analog.h"
#include "pump.h"

#define MIN_ON_TIME_MS (5000)
#define MIN_OFF_TIME_MS (5000)
#define HEAT_CHANGER_LOSS (5)

#define BOYLER_CHANNEL (0)
#define FLOOR_CHANNEL (1)

#define NOTIFY_WAIT_MS (100)

#define EVT_PUMP_START (1<<0)
#define EVT_PUMP_STOP (1<<1)

#define EVT_PUMP_MASK ((EVT_PUMP_START) | (EVT_PUMP_STOP))

typedef struct {
	uint32_t boyler_temperature;
	uint32_t floor_temperature;
	uint8_t pump_state;
	TickType_t current_time;
	TickType_t last_time;
	TickType_t last_changed_time;
} pump_control_t;

static pump_control_t pump_control;

extern osThreadId_t adTaskHandle;
extern osMutexId_t mtx_adHandle;

BaseType_t pump_init(){
	bzero(&pump_control, sizeof(pump_control_t));
	return pdTRUE;
}

BaseType_t pump_deinit(){
	return pdTRUE;
}

void pump_processing_task(void const *argument){

	for(;;){
		pump_control.current_time = HAL_GetTick();

		ad_get(&pump_control.boyler_temperature, BOYLER_CHANNEL, pdMS_TO_TICKS(NOTIFY_WAIT_MS));
		ad_get(&pump_control.floor_temperature, FLOOR_CHANNEL, pdMS_TO_TICKS(NOTIFY_WAIT_MS));

		if(pump_control.boyler_temperature > (pump_control.floor_temperature + HEAT_CHANGER_LOSS)){
			if((pump_control.current_time - pump_control.last_changed_time) > pdMS_TO_TICKS(MIN_OFF_TIME_MS)){
				if(!pump_control.pump_state){
					HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET);
					pump_control.pump_state = 1;
					pr_info("pump switched on");
					pump_control.last_changed_time = pump_control.current_time;
				}
			}
		}

		if(pump_control.boyler_temperature <= (pump_control.floor_temperature + HEAT_CHANGER_LOSS)){
			if((pump_control.current_time - pump_control.last_changed_time) > pdMS_TO_TICKS(MIN_ON_TIME_MS)){
				if(pump_control.pump_state){
					HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_RESET);
					pr_info("pump switched off");
					pump_control.pump_state = 0;
					pump_control.last_changed_time = pump_control.current_time;
				}
			}
		}

		pr_info("boyler temperature: %d, floor temperature: %d, pump state: %d",
				pump_control.boyler_temperature, pump_control.floor_temperature, pump_control.pump_state);


		osDelay(500);
	}

}

#define PUMP_HELP "help"
#define PUMP_START "start"
#define PUMP_STOP "stop"
#define PUMP_STAT "stat"

void pump_usage(){
	sh_printf("usage %s <help | start | stop | stat>\r\n", TASK_COMMAND_PUMP);
}

void cmd_pump(int argc, char **argv){

	if(argc <= 0 || strcasecmp(PUMP_HELP, argv[0]) == 0){
		pump_usage();
		return;
	}

	if(strcasecmp(PUMP_START, argv[0]) == 0){
		if(pump_control.pump_state){
			sh_printf("pump is already running\r\n");
		}
		else{
			pump_control.pump_state = 1;
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET);
			pump_control.last_changed_time = pump_control.current_time;
			sh_printf("pump is force starting\r\n");
		}
		return;
	}

	if(strcasecmp(PUMP_STOP, argv[0]) == 0){
		if(pump_control.pump_state){
			pump_control.pump_state = 0;
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_RESET);
			pump_control.last_changed_time = pump_control.current_time;
			sh_printf("pump is force stopping\r\n");
		}
		else{
			sh_printf("pump is already stopped\r\n");
		}
		return;
	}

	if(strcasecmp(PUMP_STAT, argv[0]) == 0){
		if(pump_control.pump_state){
			sh_printf("pump status is on for %d seconds\r\n",
					(HAL_GetTick() - pump_control.last_changed_time) / 1000);
		}
		else{
			sh_printf("pump status is off\r\n");
		}
		return;
	}

	pump_usage();
	return;
}

