/*
 * analog.h
 *
 *  Created on: Jan 10, 2021
 *      Author: marci
 */

#ifndef INC_ANALOG_H_
#define INC_ANALOG_H_

#include "FreeRTOS.h"

#define TASK_COMMAND_AD "ad"
#define TASK_COMMAND_TEMP "temp"

#define ANALOG_COMMANDS \
{.command_name = TASK_COMMAND_AD, .function = cmd_analog}

#define TEMPERATURE_COMMANDS \
{.command_name = TASK_COMMAND_TEMP, .function = cmd_temp}

BaseType_t ad_init();
BaseType_t ad_deinit();

void cmd_analog(int argc, char **argv);

void cmd_temp(int argc, char **argv);

void ad_processing_task(void const *argument);

uint8_t ad_get(uint32_t *result, uint8_t channel, TickType_t wait);

#endif /* INC_ANALOG_H_ */
