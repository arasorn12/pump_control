#include "FreeRTOS.h"

#define TASK_COMMAND_PUMP "pump"

#define PUMP_COMMANDS \
{.command_name = TASK_COMMAND_PUMP, .function = cmd_pump}

BaseType_t pump_init();
BaseType_t pump_deinit();

void cmd_pump(int argc, char **argv);

void pump_processing_task(void const *argument);
